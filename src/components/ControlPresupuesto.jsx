const ControlPresupuesto = ({ presupuesto }) => {
    const formatearCantidad = cantidad => {
        return cantidad.toLocaleString("en-US", {
            style: "currency",
            currency: "USD",
        });
    };

    return (
        <div className="contenedor-presupuesto contenedor sombra dos-columnas">
            <div>
                <p>Graficas</p>
            </div>
            <div className="contenido-presupuesto ">
                <p>
                    <span>Presupuesto: {formatearCantidad(presupuesto)}</span>
                </p>

                <p>
                    <span>Disponible: {formatearCantidad(0)}</span>
                </p>

                <p>
                    <span>Gastado: {formatearCantidad(0)}</span>
                </p>
            </div>
        </div>
    );
};

export default ControlPresupuesto;
